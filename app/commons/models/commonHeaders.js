
module.exports = {
    responseHeaders: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'timestamp,tz,tenant-id,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
        'Access-Control-Allow-Origin': '*',        // Required for CORS support to work
        'Access-Control-Allow-Credentials': true   // Required for cookies, authorization headers with HTTPS 
    },
    responseHtmlHeaders: {
        'Content-Type': 'text/html',
        'Access-Control-Allow-Headers': 'timestamp,tz,tenant-id,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token',
        'Access-Control-Allow-Origin': '*',        // Required for CORS support to work
        'Access-Control-Allow-Credentials': true   // Required for cookies, authorization headers with HTTPS 
    }
}
