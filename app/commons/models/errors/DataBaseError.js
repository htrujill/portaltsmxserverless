/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que gestiona el error de base de datos
 * @author          Hector Trujillo Ruiz
 * @creationDate    05 de Noviembre del 2019
 * @class Clase que se encarga de extender la clase Error para obtener los errores de base de datos
 */
class DataBaseError extends Error {
    /**
     * Constructor de la clase
     * @param {String} message Mensaje de error
     * @param {Object} stack Seguimiento del error
     */
    constructor(message, stack) {
        super(message);
        this.name = "DataBaseError";
        this.stack = stack;
    }
}
/// Exporta la clase
module.exports = DataBaseError;