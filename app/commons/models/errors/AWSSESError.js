/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que gestiona un error generado por SES de AWS
 * @author          Hector Trujillo Ruiz
 * @creationDate    20 de Enero del 2020
 * @class           Clase que gestiona un error generado por SES de AWS
 */
class AWSSESError extends Error {
    /**
     * Constructor de la clase
     * @param {String} message Mensaje de error
     * @param {Object} stack Seguimiento del error
     */
    constructor(message, stack) {
        super(message);
        this.name = "AWSSESError";
        this.stack = stack;
    }
}
/// Exporta la clase
module.exports = AWSSESError;