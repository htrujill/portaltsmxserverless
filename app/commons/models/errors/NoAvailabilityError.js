/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que gestiona el error que indica que un item no tiene disponibilidad para reservar
 * @author          Hector Trujillo Ruiz
 * @creationDate    12 de Noviembre del 2019
 * @class Clase que gestiona el error que indica que un item no tiene disponibilidad para reservar
 */
class NoAvailabilityError extends Error {
    /**
     * Constructor de la clase
     * @param {String} message Mensaje de error
     * @param {Object} stack Seguimiento del error
     */
    constructor(message, stack) {
        super(message);
        this.name = "NoAvailabilityError";
        this.stack = stack;
    }
}
/// Exporta la clase
module.exports = NoAvailabilityError;