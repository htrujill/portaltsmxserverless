/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Entidad que contiene la información de respuesta de las lambdas en general
 * @author          Hector Trujillo Ruiz
 * @creationDate    28 de Octubre del 2019
 */
/**
 * Entidad que contiene la información de la respuesta genérica de las funciones lambda
 * @param {Number} statusCode Código HTTP de respuesta
 * @param {Object} headers Headers a devolver en la respuesta
 * @param {Object} body Cuerpo de la respuesta a serializar
 */
function commonResponse(statusCode, headers, body) {
    this.statusCode = statusCode;
    this.headers = headers;
    this.body = body == null ? JSON.stringify({}) : JSON.stringify(body);
}
/**
 * Obtiene el código HTTPs
 * @returns {Number} Código de la respuesta HTTPS
 */
commonResponse.prototype.getStatusCode = function () {
    return this.statusCode;
}
/**
 * Asigna el código HTTP de la respuesta
 * @param {Number} statusCode Código de status HTTP
 */
commonResponse.prototype.setStatusCode = function (statusCode) {
    this.statusCode = statusCode;
}
/**
 * Obtiene los headers de la respuesta
 * @returns {Object} Headers de la respuesta
 */
commonResponse.prototype.getHeaders = function () {
    return this.headers;
}
/**
 * Asigna los headers a la respuesta
 * @param {Object} headers Headers de la respuesta de las lambdas
 */
commonResponse.prototype.setHeaders = function (headers) {
    this.headers = headers;
}
/**
 * Obtiene el cuerpo de la respuesta
 * @returns {Object} Cuerpo de la respuesta
 */
commonResponse.prototype.getBody = function () {
    return JSON.parse(this.body);
}
/**
 * Asigna el cuerpo de la respuesta
 * @param {Object} body Cuerpo de la respuesta
 */
commonResponse.prototype.setBody = function (body) {
    this.body = JSON.stringify(body);
}
/// Exporta el objeto
module.exports = commonResponse;