/**
 * Entidad base que contiene las propiedades comunes
 */
const BaseEntity = require('../BaseEntity');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene la información de un mapa
 * @author          Hector Trujillo Ruiz
 * @creationDate    11 de Noviembre del 2019
 * @class Clase que contiene la información de un mapa
 */
class Map extends BaseEntity {
    /**
     * Constructor de la clase
     * @param {Number} id Id de la ubicación
     * @param {String} name Nombre de la ubicación
     * @param {String} comments Comentarios
     * @param {Date} lastUpdate Fecha de la última actialización
     * @param {String} updatedBy Fecha de la ultima actualización en cadena
     * @param {Number} status Estado de la ubicación
     * @param {Number} locationId Id de la ubicación
     * @param {Number} locationId Id de la ubicación
     * @param {Number} resourceTypeId Id de tipo de recurso
     */
    constructor(id, name, locationId, comments, lastUpdate, updatedBy, status, resource, resourceTypeId, content) {
        super(id, comments, lastUpdate, updatedBy, status);
        this.name = name;
        this.locationId = locationId;
        this.resource = resource;
        this.resourceTypeId = resourceTypeId;
        this.content = content;
    }

	/**
	 * Obtiene el nombre de la ubicación
	 * @returns {String} Nombre de la ubicación
	 */
    getName() {
        return this.name;
    }

	/**
	 * Asigna el nombre de la ubicación
	 * @param {String} name Nombre de la ubicación
	 */
    setName(name) {
        this.name = name;
    }

	/**
	 * Obtiene el Id de la ubicación
	 * @returns {Number} Id de la ubicación
	 */
    getLocationId() {
        return this.locationId;
    }

	/**
	 * Asigna el Id de la ubicación
	 * @param {Number} locationId Id de la ubicación
	 */
    setLocationId(locationId) {
        this.locationId = locationId;
    }

	/**
	 * Obtiene el nombre de la ubicación
	 * @returns {String} Nombre de la ubicación
	 */
    getResource() {
        return this.resource;
    }

	/**
	 * Asigna el nombre de la ubicación
	 * @param {String} name Nombre de la ubicación
	 */
    setResource(resource) {
        this.resource = resource;
    }

	/**
	 * Obtiene el id del tipo de recurso
	 * @returns {Number} Id del tipo de recurso
	 */
    getResourceTypeId() {
        return this.resourceTypeId;
    }

	/**
	 * Obtiene el id del tipo de recurso
	 * @param {Number} resourceTypeId Id del tipo de recurso
	 */
    setResourceTypeId(resourceTypeId) {
        this.resourceTypeId = resourceTypeId;
    }

	/**
	 * Obtiene el id del tipo de recurso
	 * @returns {Number} Id del tipo de recurso
	 */
    getContent() {
        return this.content;
    }

	/**
	 * Obtiene el id del tipo de recurso
	 * @param {String} content Contenido
	 */
    setContent(content) {
        this.content = content;
    }
}
/// Exporta la clase
module.exports = Map;