/**
 * Entidad base que contiene las propiedades comunes
 */
const BaseEntity = require('../BaseEntity');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene la información de un Usuario
 * @author          Hector Trujillo Ruiz
 * @creationDate    09 de Noviembre del 2019
 * @class           Clase que contiene la información de un Usuario
 */
class Resource extends BaseEntity {
    /**
     * Constructor de la clase
     * @param {Number} id Id del recurso
     * @param {Number} resourceTypeId Id del tipo de recurso
     * @param {String} content Contenido del recurso
     * @param {Date} lastUpdate Fecha de la última actualización
     * @param {String} updatedBy Usuario que actualiza
     */
    constructor(id, resourceTypeId, content, lastUpdate, updatedBy) {
        super(id, null, lastUpdate, updatedBy, null);
        this.resourceTypeId = resourceTypeId;
        this.content = content;
    }

    getResourceTypeId() {
        return this.resourceTypeId;
    }

    setResourceTypeId(resourceTypeId) {
        this.resourceTypeId = resourceTypeId;
    }

    getContent() {
        return this.content;
    }

    setContent(content) {
        this.content = content;
    }
}
/// Exporta el modulo
module.exports = Resource;
