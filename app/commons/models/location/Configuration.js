/**
 * Entidad base que contiene las propiedades comunes
 */
const BaseEntity = require('../BaseEntity');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene la configuración de una ubicación
 * @author          Hector Trujillo Ruiz
 * @creationDate    11 de Noviembre del 2019
 * @class Clase que contiene la configuración de una ubicación
 */
class Configuration extends BaseEntity {
    /**
     * Constructor de la clase
     * @param {Number} minutes Nombre de la ubicación
     * @param {Number} itemTypeId Id del tipo de artículo al que pertenece la configuración
     * @param {String} comments Comentarios
     * @param {String} name Nombre del tipo de artículo
     * @param {[]} attributes Lista de atributos de una sala
     */
	constructor( minutes, itemTypeId, comments, name, attributes) {
		super(null, comments, null, null, null);
		this.minutes = minutes;
		this.itemTypeId = itemTypeId;
		this.name = name;
		this.attributes = attributes;
	}

	/**
	 * Obtiene los minutes de la configuración
	 * @returns {String} Nombre de la ubicación
	 */
	getMinutes() {
		return this.minutes;
	}

	/**
	 * Asigna los minutos de la configuración
	 * @param {String} minutes Nombre de la ubicación
	 */
	setMinutes(minutes) {
		this.minutes = minutes;
	}

	/**
	 * Obtiene el id del tipo de artículo al que pertenece la configuración
	 * @returns {Number} id del tipo de artículo
	 */
	getItemTypeId() {
		return this.itemTypeId;
	}

	/**
	 * Asigna el id del tipo de artículo al que pertenece la configuración
	 * @param {Number} itemTypeId id del tipo de artículo
	 */
	setItemTypeId(itemTypeId) {
		this.itemTypeId = itemTypeId;
	}

	/**
	 * Obtiene el nombre del tipo de artículo
	 * @returns {String} Nombre del tipo de artículo
	 */
	getName() {
		return this.name;
	}

	/**
	 * Asigna el nombre del tipo de artículo
	 * @param {String} name Nombre del tipo de artículo
	 */
	setName(name) {
		this.name = name;
	}

	/**
	 * Obtiene el nombre del tipo de artículo
	 * @returns {String} Nombre del tipo de artículo
	 */
	getAttributes() {
		return this.attributes;
	}

	/**
	 * Asigna el nombre del tipo de artículo
	 * @param {String} attributes Lista de atributos del item
	 */
	setAttributes(attributes) {
		this.attributes = attributes;
	}
}
/// Exporta la clase
module.exports = Configuration;