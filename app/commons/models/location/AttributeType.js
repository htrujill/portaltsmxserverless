/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene la información de un atributo
 * @author          Hector Trujillo Ruiz
 * @creationDate    13 de Enero del 2020
 * @class           Clase que contiene la información de un atributo
 */
class AttributeType {
    /**
     * 
     * @param {Number} attributeType Id del tipo de atributo
     * @param {Number} minValue Valor mínimo
     * @param {Number} maxValue Valor máximo
     * @param {String} name Nombre
     * @param {String} description Descripción
     */
    constructor(id, attributeType, minValue, maxValue, name, description, values) {
        this.id = id;
        this.attributeType = attributeType;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.name = name;
        this.description = description;
        this.values = values;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getAttributeTypeType() {
        return this.attributeType;
    }

    setAttributeTypeType(attributeType) {
        this.attributeType = attributeType;
    }

    getMinValue() {
        return this.minValue;
    }

    setMinValue(minValue) {
        this.minValue = minValue;
    }

    getMaxValue() {
        return this.maxValue;
    }

    setMaxValue(maxValue) {
        this.maxValue = maxValue;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getDescription() {
        return this.description;
    }

    setDescription(description) {
        this.description = description;
    }

    getValues() {
        return this.values;
    }

    setValues(values) {
        this.values = values;
    }
}

module.exports = AttributeType;