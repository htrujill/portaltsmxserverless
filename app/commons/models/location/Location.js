/**
 * Entidad base que contiene las propiedades comunes
 */
const BaseEntity = require('../BaseEntity');
/**
 * Configuración de la ubicación
 */
const Configuration = require('./Configuration');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene la información de una ubicación
 * @author          Hector Trujillo Ruiz
 * @creationDate    05 de Noviembre del 2019
 * @class Clase que contiene la información de una ubicación
 */
class Location extends BaseEntity {
    /**
     * Constructor de la clase
     * @param {Number} id Id de la ubicación
     * @param {String} name Nombre de la ubicación
     * @param {String} comments Comentarios
     * @param {Date} lastUpdate Fecha de la última actialización
     * @param {String} updatedBy Fecha de la ultima actualización en cadena
     * @param {Number} status Estado de la ubicación
     * @param {[Configuration]} configurations Configuración de la ubicación
     */
	constructor(id, name, comments, lastUpdate, updatedBy, status, configurations) {
		super(id, comments, lastUpdate, updatedBy, status);
		this.name = name;
		this.configurations = configurations;
	}

	/**
	 * Obtiene el nombre de la ubicación
	 * @returns {String} Nombre de la ubicación
	 */
	getName() {
		return this.name;
	}

	/**
	 * Asigna el nombre de la ubicación
	 * @param {String} name Nombre de la ubicación
	 */
	setName(name) {
		this.name = name;
	}

	/**
	 * Obtiene la configuración de la ubicación
	 * @returns {[Configuration]} Configuración
	 */
	getConfigurations() {
		return this.configurations;
	}

	/**
	 * Asigna la configuración
	 * @param {[Configuration]} configurations Configuración
	 */
	setConfigurations(configurations) {
		this.configurations = configurations;
	}
}
/// Exporta la clase
module.exports = Location;