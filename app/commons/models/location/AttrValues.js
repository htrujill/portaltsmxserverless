/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene los valores de un atributo
 * @author          Hector Trujillo Ruiz
 * @creationDate    13 de Enero del 2020
 * @class           Clase que contiene los valores de un atributo
 */
class AttrValue {
    /**
     * Constructor de la clase
     * @param {Number} nValue Valor numérico
     * @param {String} sValue Valor en cadena
     * @param {String} name Nombre 
     */
    constructor(nValue, sValue, name) {
        this.nValue = nValue;
        this.sValue = sValue;
        this.name = name;
    }

    getNValue() {
        return this.nValue;
    }

    setNValue(nValue) {
        this.nValue = nValue;
    }

    getSValue() {
        return this.sValue;
    }

    setSValue(sValue) {
        this.sValue = sValue;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }
}
/// Exporta el módulo
module.exports = AttrValue;
