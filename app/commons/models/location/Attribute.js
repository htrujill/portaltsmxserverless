/**
 * Entidad base que contiene las propiedades comunes
 */
const BaseEntity = require('../BaseEntity');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene la información de un atributo de un item
 * @author          Hector Trujillo Ruiz
 * @creationDate    14 de Enero del 2020
 * @class           Clase que contiene la información de un atributo de un item
 */
class Attribute extends BaseEntity {
    /**
     * Constructor de la clase
     * @param {Number} id Id del atributp
     * @param {Number} itemTypeId Id del tipo de atributo
     * @param {Number} attributeType Tipo de atributo
     * @param {Number} minValue Valor mínimo
     * @param {Number} maxValue Valor máximo
     * @param {String} name Nombre del atributo
     * @param {String} description Descripción del atributo
     * @param {Date} lastUpdate Fecha de actualización
     * @param {String} updatedBy Usuario que actualiza
     * @param {Number} resourceType Tipo de recurso
     * @param {String} content Contenido
     */
    constructor(id, itemTypeId, attributeType, minValue, maxValue, name, description, lastUpdate, updatedBy, resourceType, content, nVal, sVal) {
        super(id, null, lastUpdate, updatedBy, null);
        this.itemTypeId = itemTypeId;
        this.attributeType = attributeType;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.name = name;
        this.description = description;
        this.resourceType = resourceType;
        this.content = content;
        this.nVal = nVal;
        this.sVal = sVal;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getItemTypeId() {
        return this.itemTypeId;
    }

    setItemTypeId(itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    getAttributeType() {
        return this.attributeType;
    }

    setAttributeType(attributeType) {
        this.attributeType = attributeType;
    }

    getMinValue() {
        return this.minValue;
    }

    setMinValue(minValue) {
        this.minValue = minValue;
    }

    getMaxValue() {
        return this.maxValue;
    }

    setMaxValue(maxValue) {
        this.maxValue = maxValue;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getDescription() {
        return this.description;
    }

    setDescription(description) {
        this.description = description;
    }

    getLastUpdate() {
        return this.lastUpdate;
    }

    setLastUpdate(lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    getUpdatedBy() {
        return this.updatedBy;
    }

    setUpdatedBy(updatedBy) {
        this.updatedBy = updatedBy;
    }

    getResourceType() {
        return this.resourceType;
    }

    setResourceType(resourceType) {
        this.resourceType = resourceType;
    }

    getContent() {
        return this.content;
    }

    setContent(content) {
        this.content = content;
    }

    getNVal() {
        return this.nVal;
    }

    setnVal(nVal) {
        this.nVal = nVal;
    }

    getSVal() {
        return this.sVal;
    }

    setSVal(sVal) {
        this.sVal = sVal;
    }
};
/// Exporta el módulo
module.exports = Attribute;
