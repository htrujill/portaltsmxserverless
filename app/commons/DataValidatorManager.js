/**
 * Clase para gestionar los errores de validación de tipos de datos
 */
let DataValidationError = require('../commons/models/errors/DataValidationError');
/**
 * 
 */
const prototypeFunctions = require('./functions/PrototypeFunctions');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Manager que valida la información/parámetros de entrada
 * @author          Hector Trujillo Ruiz
 * @creationDate    10 de Noviembre del 2019
 * @class Manager que valida la información/parámetros de entrada
 */
let dataValidatorHandler = {};
/**
 * Método que valida que el token de autorización y el usuario del que se está solicitando la información sea el mismo
 * @param {Object} event Evento de solicitud de la lambda, enviado por API Gateway
 * @param {String} username Número de control del usuario
 * @returns {void} Sí es válido continua con el flujo
 * @throws {Error} Sí el usuario no concuerda, se lanza la exepción
 */
dataValidatorHandler.validateAuthToken = (event, username) => {
    // // console.log("Validación deshabilitada temporalmente para pruebas.");
    // console.log("Validating auth token.");
    // /// Obtiene el usuario autenticado en cognito
    // const tokenUsername = event.requestContext.authorizer.claims['cognito:username'];
    // /// Valida los usuarios, sí alguno de los dos no se proporciona o sí no coinciden
    // if (tokenUsername != username || tokenUsername == null || username == null) {
    //     throw new Error("The user you are trying to consult the information does not correspond to the authenticated user, please verify.");
    // }
    // console.log("User validation ok.");
    // return tokenUsername;
};
/**
 * Método que obtiene el usuario que está autenticado
 * @param {Object} event Evento de la lambda
 * @returns {String} usuario autenticado
 */
dataValidatorHandler.getAuthUsername = (event) => {
    return 'A52428577';
    // return event.requestContext.authorizer.claims['cognito:username'];
};
/**
 * Método que evalua sí un parámetro es númerico y lo devuelve como entero
 * @param {String | Number} value Valor a evaluar y convertir en entero
 * @returns {number} Valor convertido a entero
 */
dataValidatorHandler.validateInteger = (value) => {
    console.log("Validating parameter as integer");
    let returnValue = parseInt(value);
    if (value === null || value === undefined || isNaN(returnValue) || isNaN(Number(value))) {
        throw new DataValidationError(`The value provided is not a valid number.`);
    };
    console.log("It is a valid number.");
    return returnValue;
};
/**
 * Método que valida que una cadena no sea nula y que tenga la longitud mínima establecida
 * @param {String} value Cadena a evaluar
 * @param {Number} length Longitud a evaluar
 * @returns {String} Valor enviado como parámetro
 * @throws {DataValidationError} Excepción lanzada en caso de que no sea una cadena o que la cadena no cuente con la longitud mínima necesaria
 */
dataValidatorHandler.validateString = (value, length) => {
    console.log("Validating a string.");
    console.log("value: ", value);
    console.log("length: ", length);
    /// Sí la cadena es nula
    if (value == null) {
        throw new DataValidationError(`A valid string value should be provided.`);
    }
    /// Sí no cuenta con la longitud mínima
    if (value.length < length) {
        throw new DataValidationError(`The minimum string length must be ${length}`);
    }
    console.log("It is a valid string.");
    return value;
};
/**
 * Método que convierte/valida un Unix epoch time a fecha
 * @param {Number} value Unix epoch time
 * @returns {Date} Fecha enviada
 */
dataValidatorHandler.validateDate = (value) => {
    console.log("Value date: ", value);
    /// Convierte el valor en fecha
    let returnDate = new Date();
    returnDate.setUnixTime(value);
    /// Sí la fecha no es válida
    if (!returnDate.isValid()) {
        throw new DataValidationError(`The date value provided should be a valid Unix epoch time.`);
    }
    console.log("Is a valid date: ", returnDate);
    /// Devuelve la fecha
    return returnDate;
};
/**
 * Método que valida que un valor Booleano
 * @param {Boolean} value Valor booleano válido
 * @returns {Boolean} Valor que se provee sí es un booleano válido.
 * @throws {DataValidationError} Excepción lanzada en caso de que no sea un valor booleano válido 
 */
dataValidatorHandler.validateBoolean = (value) => {
    console.log(typeof value);
    /// Si no se provee ningun valor o no es un booleano
    if (typeof value == 'undefined' || value == null || typeof value != 'boolean') {
        console.log(`The value ${value} should be a valid boolean.`);
        // throw new DataValidationError(`The value ${value} should be a valid boolean.`);
    }
    return value;
};
/// Exporta el módulo
module.exports = dataValidatorHandler;
