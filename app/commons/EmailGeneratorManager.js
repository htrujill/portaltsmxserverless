/**
 * Paquete para la generación de invitaciones al calendario
 */
const ics = require('ics');
/**
 * Genera un mensaje en MIME Type
 */
let mimemessage = require('mimemessage');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Manager que genera correos electrónicos con ICAL
 * @author          Hector Trujillo Ruiz
 * @creationDate    13 de Noviembre del 2019
 * @class Manager que genera correos electrónicos con ICAL
 */
let emailGeneratorManager = {};
/**
 * Método que genera un mensaje tipo MIME con Sesiones de iCalendar
 * @param {String} HTMLContent Contenido HTML del mensaje
 * @param {[Object]} Sessions Sesiones para crear el ICalendar
 * @param {String} Subject Asunto del email
 * @param {Number} MessageID Id del mensaje
 * @param {String} Sender Persona que envía el mensaje, debe ser el organizador de las sesiones sí es que existe
 * @returns {String} Mensaje en formato MIME 
 */
emailGeneratorManager.generateMimeMessageWithICal = async (HTMLContent, Sessions, Subject, MessageID, Sender) => {
    /// Mensaje de retorno
    let returnMessage = mimemessage.factory({
        contentType: 'multipart/mixed;charset=utf-8',
        body: []
    });
    /// Agrega el header
    returnMessage.header('Subject', Subject);
    // returnMessage.header('Categories', 'MEETING');
    /// Si se asigna el id el mensaje
    if (MessageID)
        returnMessage.header('Message-ID', MessageID);
    /// Si se asigna el que envía el mensaje
    if (Sender)
        returnMessage.header('Sender', Sender);
    // returnMessage.header('Content-Class', 'urn:content-classes:calendarmessage');
    /// Crea el mensaje que contiene todas las entidades
    let alternateEntity = mimemessage.factory({
        contentType: 'multipart/alternative;charset=utf-8',
        body: []
    });
    // Build the HTML MIME entity.
    let htmlEntity = mimemessage.factory({
        contentType: 'text/html;charset=utf-8',
        body: HTMLContent
    });
    /// Se agrega el contenido HTML
    alternateEntity.body.push(htmlEntity);
    for (const key in Sessions) {
        if (Sessions.hasOwnProperty(key)) {
            /// Genera las sesiones de ical
            const calendarSessions = await ics.createEvent(Sessions[key], (error, value) => {
                console.log("CreateEvent value: ", value);
                // Sí se encuentra un error
                if (error) {
                    console.log("CreateEvent error: ", error);
                    throw error;
                }
                return value;
            });
            console.log("calendarSessions: ", calendarSessions);
            /// Se agrega el calendario
            if (calendarSessions) {
                /// Crea la entidad con la sesión
                let calendarEntity = mimemessage.factory({
                    contentType: 'text/calendar; charset="utf-8"; method=REQUEST',
                    body: (Buffer.from(calendarSessions)).toString('base64')
                });
                // calendarEntity.header('Content-Class', 'urn:content-classes:calendarmessage');
                // calendarEntity.header('Content-ID', 'calendar_message');
                // /// Agrega el nombre del archivo ics como header
                // calendarEntity.header('Content-Disposition', 'attachment;filename="Clase.ics"');
                calendarEntity.header('Content-Transfer-Encoding', 'base64');
                /// Agrega la entidad al mensaje
                alternateEntity.body.push(calendarEntity);
                // returnMessage.body.push(calendarEntity);
            }
        }
    }
    // Add the multipart/alternate entity to the top-level MIME message.
    returnMessage.body.push(alternateEntity);
    console.log('returnMessage.toString(): ', returnMessage.toString());
    return returnMessage.toString();
};
/// Exporta el modulo
module.exports = emailGeneratorManager;
