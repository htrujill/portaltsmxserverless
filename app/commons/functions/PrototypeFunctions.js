/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Funciones agregadas a los prototypes de los tipos de datos nativos
 * @author          Hector Trujillo Ruiz
 * @creationDate    03 de Junio del 2019
 */
/**
 * Método que agrega una función al prototype de Date
 * Obtiene la fecha en formato Unix Timestamp
 */
Date.prototype.getUnixTime = function () {
    return this.getTime() / 1000;
};
/**
 * Método que inicializa la fecha desde un Epoch unix time
 * @param {Number} epochUnixTime Unix time para inicializar la fecha
 */
Date.prototype.setUnixTime = function (epochUnixTime) {
    /// Inicializa la fecha con el epoch unix time y lo multiplica por 1000
    this.setTime(epochUnixTime * 1000);
};

/// Formato de la fecha a string 
Date.prototype.toStringISO8601 = function () {
    // let d = this;
    let month = '' + (this.getMonth() + 1);
    let day = '' + this.getDate();
    let year = this.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};
/**
 * Método que valida sí una fecha es válida
 */
Date.prototype.isValid = function () {
    let time = this.getTime();
    // If the date object is invalid it 
    // will return 'NaN' on getTime()  
    // and NaN is never equal to itself. 
    return this.getTime() === time;
};
/**
 * Método que agrega la cantidad de días especificados a la fecha proporcionada
 * @param {Number} days Número de días a agregar
 */
Date.prototype.addDays = function (days) {
    this.setDate(this.getDate() + days);
};
/**
 * Método que agrega la cantidad de días especificados a la fecha proporcionada
 * @param {Number} days Número de días a agregar
 */
Date.prototype.addMonths = function (months) {
    this.setMonth(this.getMonth() + months);
};