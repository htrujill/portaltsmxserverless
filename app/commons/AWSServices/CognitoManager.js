/// Inicializa la variable de AWS
const AWS = require('aws-sdk');
/// Variable de cognito
const CognitoISP = new AWS.CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18'
});
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Capa de conexión a cognito
 * @author          Hector Trujillo Ruiz
 * @creationDate    06 de Septiembre del 2019
 * @class           Clase que se encarga de la conexión a cognito
 */
module.exports = {
    /**
     * Método que obtiene la información de un usuario de cognito como admin
     * @param {CognitoIdentityServiceProvider.Types.AdminGetUserRequest} params Parámetros
     * @returns {Object} Información del usuario
     */
    adminGetUser: async (params) => {
        /// Obtiene la información de un usuario de cognito
        return await CognitoISP.adminGetUser(params).promise().then(function (data) {
            console.log("adminGetUser data", data);
            return data;
        }).catch(function (error) {
            console.error("adminGetUser error", error);
            throw new Error('LCE005');
        });
    },
    /**
     * Método que actualiza la información de los atributos de un usuario
     * @param {CognitoIdentityServiceProvider.Types.AdminUpdateUserAttributesRequest} params información del usuario
     * @returns {Object} Info
     */
    adminUpdateUserAttributes: async (params) => {
        /// Actualiza el número de teléfono
        return await CognitoISP.adminUpdateUserAttributes(params).promise().then(function (data) {
            console.log("adminUpdateUserAttributes data", data);
            return data;
        }).catch(function (error) {
            console.error("adminUpdateUserAttributes error", error);
            /// No es posible actualizar la iformación de un usuario
            throw new Error('LCE007');
        });
    },
    /**
     * Método que obliga al usuario a cambiar su contraseña
     * @param {CognitoIdentityServiceProvider.Types.AdminResetUserPasswordRequest} params parámetros
     * @returns {Object} Respuesta
     */
    adminResetUserPassword: async (params) => {
        return await CognitoISP.adminResetUserPassword(params).promise().then(function (data) {
            console.log("adminResetUserPassword data", data);
            return data;
        }).catch(function (error) {
            console.error("adminResetUserPassword error", error);
            /// No es posible actualizar la iformación de un usuario
            throw new Error('LCE007');
        });
    },
    /**
     * Método que confirma la inscripción de un usuario
     * @param {CognitoIdentityServiceProvider.Types.AdminResetUserPasswordRequest} params parámetros
     * @returns {Object} Respuesta
     */
    adminConfirmSignUp: async (params) => {
        return await CognitoISP.adminConfirmSignUp(params).promise().then(function (data) {
            console.log("adminConfirmSignUp data", data);
            return data;
        }).catch(function (error) {
            console.error("adminConfirmSignUp error", error);
            /// No es posible actualizar la iformación de un usuario
            throw new Error('LCE007');
        });
    },
    /**
     * Método que registra un usuario en Cognito
     * @param {CognitoIdentityServiceProvider.Types.SignUpRequest} params Parámetros para crear el usuario
     * @returns {Object} Respuesta del signup
     */
    signUp: async (params) => {
        return await CognitoISP.signUp(params).promise().then(function (data) {
            console.log("signUp data", data);
            return data;
        }).catch(function (error) {
            console.error("signUp error", error);
            /// No es posible actualizar la iformación de un usuario
            throw new Error('LCE007');
        });
    },
    /**
     * Método que crea un usuario con una contraseña temporal
     * @param {CognitoIdentityServiceProvider.Types.AdminCreateUserRequest} params Parámetros para crear un usuario
     * @returns {Object} Respuesta
     */
    adminCreateUser: async (params) => {
        return await CognitoISP.adminCreateUser(params).promise().then(function (data) {
            console.log("adminCreateUser data", data);
            return data;
        }).catch(function (error) {
            console.error("adminCreateUser error", error);
            /// No es posible crear un usuario
            throw new Error('LCE007');
        });
    },
    /**
     * Método que establece la contraseña del usuario como admin
     * @param {CognitoIdentityServiceProvider.Types.AdminSetUserPasswordRequest} params Parámetros para establecer la contraseña
     * @returns {Object} Respuesta
     */
    adminSetUserPassword: async (params) => {
        return await CognitoISP.adminSetUserPassword(params).promise().then(function (data) {
            console.log("adminSetUserPassword data", data);
            return data;
        }).catch(function (error) {
            console.error("adminSetUserPassword error", error);
            /// No es posible crear un usuario
            throw new Error('LCE007');
        });
    }
}