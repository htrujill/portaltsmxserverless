/// Inicializa la variable de AWS
const AWS = require('aws-sdk');
/// Actualiza la región por ladel proyecto
AWS.config.update({
    region: process.env.REGION_LAMBDA
});
/// Inicializa la variable de SQS
const SQS = new AWS.SQS({
    endpoint: 'https://vpce-075a1dc243bd917a3-hhuuazdn.sqs.us-west-2.vpce.amazonaws.com'
});
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Capa de gestión de queues de SQS
 * @author          Hector Trujillo Ruiz
 * @creationDate    25 de Julio del 2019
 *
 * @class Clase que se encarga de la gestión de queues de SQS
 */
module.exports = {
    /**
     * Método que envía un mensaje a una queue
     * @param {String} Message Mensaje que se envía a la queue
     * @param {String} Url Url de la queue a la que se envía el mensaje
     * @param {Object} Attributes Atributos del mensaje
     * @returns {Object} Respuesta
     */
    sendMessage: (Message, Url, Attributes) => {
        console.log("Message: ", Message);
        console.log("Url: ", Url);
        console.log("Attributes: ", Attributes);
        /// Envía un mensaje a una queue y espera la respuesta
        return SQS.sendMessage({
            MessageBody: Message,
            QueueUrl: Url,
            MessageAttributes: Attributes
        }).promise().then(function (data) {
            /// Sí todo salio bien, se imprime y se devuelve la respuesta
            console.log(`Message sent to queue: `, data);
            return data;
        }).catch(function (err) {
            console.log(`Error sending message to queue ${Url}: `, err);
            ///throw err;
        });
    },
    /**
     * Método que envía un mensaje a una queue
     * @param {String} Message Mensaje que se envía a la queue
     * @param {String} Url Url de la queue a la que se envía el mensaje
     * @param {Object} Attributes Atributos del mensaje
     * @returns {Object} Respuesta
     */
    sendFIFOMessage: (Message, Url, Attributes, MessageGroupId ) => {
        console.log("Message: ", Message);
        console.log("Url: ", Url);
        console.log("Attributes: ", Attributes);
        console.log("MessageGroupId: ", MessageGroupId);
        /// Envía un mensaje a una queue y espera la respuesta
        return SQS.sendMessage({
            MessageBody: Message,
            QueueUrl: Url,
            MessageAttributes: Attributes,
            MessageGroupId : MessageGroupId 
        }).promise().then(function (data) {
            /// Sí todo salio bien, se imprime y se devuelve la respuesta
            console.log(`Message sent to queue: `, data);
            return data;
        }).catch(function (err) {
            console.log(`Error sending message to queue ${Url}: `, err);
            ///throw err;
        });
    }
}
