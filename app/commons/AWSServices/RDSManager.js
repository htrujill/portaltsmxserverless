/// Inicializa la variable de AWS
const AWS = require('aws-sdk');
/// Inicializa la variable de DynamiDB
const DynamoDB = new AWS.RDSDataService({
    region: process.env.REGION_LAMBDA
});
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Capa de conexión a la base de datos de RDS
 * @author          Hector Trujillo Ruiz
 * @creationDate    25 de Julio del 2019
 *
 * @class Clase que se encarga de la conexión a la base de datos de RDS
 */
module.exports = {
    
}