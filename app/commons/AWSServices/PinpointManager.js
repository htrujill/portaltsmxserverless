/// Inicializa la variable de AWS
const AWS = require('aws-sdk');
/// Inicializa la variable de Pinpoint
const Pinpoint = new AWS.Pinpoint({
    region: process.env.REGION_PINPOINT
});
/// Variable para el envío de emails 
let PinpointEmail = new AWS.PinpointEmail({
    region: process.env.REGION_PINPOINT
});
/// Id de la aplicación de pinpoint
const PinpointAppId = process.env.PINPOINT_APP_ID;
/**
 * Clase que contiene la información de un error de envío de email
 */
const AWSSESError = require('../models/errors/AWSSESError');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Capa de conexión a la base de datos de DynamoDB
 * @author          Hector Trujillo Ruiz
 * @creationDate    25 de Julio del 2019
 *
 * @class Clase que se encarga de la conexión a la base de datos de DynamoDB
 */
module.exports = {
    /**
     * Método que crea un segmento de pinpoint para SegmentGroups
     * @param {String} SegmentName Nombre del segmento que se va a crear
     * @param {String} Include Que dispositivos incluye (ALL)
     * @param {Object} Attributes Atributos del segmento, se usa para definir a que criterio responde
     * @param {[Key:string, string]} Tags Tags que se asignan al segmento
     * @param {String} RecencyDuration HR_24 | DAY_7 | DAY_14 | DAY_30. Duración a usar para determinar sí un endpoint está activo o no
     * @param {String} RecencyType ACTIVE | INACTIVE. Tipo de actualidad/disponibilidad de los endpoints activos o no dentro de la duración especificada. Se debe especificar con RecencyDuration.
     * @returns {Object} Pinpoint response
     */
    createSegmentOfSegmentGroupsAttributes: (SegmentName, Include, Attributes, Tags, RecencyDuration, RecencyType) => {
        let params = {
            /// Id de la aplicación de pinpoint
            ApplicationId: PinpointAppId,
            /// Sección para crear el segmento
            WriteSegmentRequest: {
                /// Nombre del segmento
                Name: SegmentName,
                /// Grupo del segmento
                SegmentGroups: {
                    Include: Include,
                    Groups: [{
                        Type: "ANY",
                        Dimensions: [{
                            /// Atributos, define el nombre y valor
                            Attributes: Attributes
                            // ,
                            // Behavior: {
                            //     Recency: {
                            //         Duration: RecencyDuration,
                            //         RecencyType: RecencyType
                            //     }
                            // }
                        }],
                        SourceType: "ANY"
                    }]
                },
                tags: Tags
            }
        };

        console.log("params.WriteSegmentRequest.SegmentGroups: ", params.WriteSegmentRequest.SegmentGroups);

        /// Crea un segmento en pinpoint 
        return Pinpoint.createSegment(params).promise().then(function (data) {
            /// Sí todo salio bien, se imprime y se devuelve la respuesta
            // console.log(`Create pinpoint segment success ${SegmentName}: `, data);
            return data;
        }).catch(function (err) {
            // console.log(`Error creating pinpoint segment ${SegmentName}: `, err);
            throw err;
        });
    },
    /**
     * Método que crea una campaña y envía una notificación push al segmento especificado
     * @param {String} Name Nombre de la campaña
     * @param {String} Description Descripción de la campaña
     * @param {Number} SegmentId Id del segmento al que se envía la notificación
     * @param {String} SegmentVersion Versión del segmento
     * @param {Object} Schedule Información de cuando enviar la notificación
     * @param {Object} GCMMessage Mensaje para GCMM
     * @param {Object} APNSMessage Mensaje para APN
     * @param {Object} Tags Tags con los cuales se crea la campaña
     * @param {Boolean} SendSMS Bandera que indica sí se envía el SMS
     * @param {Object} SMSMessage Mensaje SMS
     * @param {Boolean} SendPush Indica sí se envía la notificación PUSH
     * @returns {Object} Respuesta
     */
    createCampaign: (Name, Description, SegmentId, SegmentVersion, Schedule, GCMMessage, APNSMessage, Tags, SendSMS, SMSMessage, SendPush) => {
        let campaignParams = {
            /// Id de la aplicación
            ApplicationId: PinpointAppId,
            /// Sección que escribe la campaña
            WriteCampaignRequest: {
                MessageConfiguration: new Object(),
                // {
                //     // /// Mensaje para APNS
                //     // APNSMessage: APNSMessage,
                //     // /// Mensaje para GCM
                //     // GCMMessage: GCMMessage
                // },
                /// Configuración para el envío
                Schedule: Schedule,
                /// Id del segmento al que se envía
                SegmentId: SegmentId,
                /// Versión del segmento
                SegmentVersion: SegmentVersion,
                AdditionalTreatments: [],
                HoldoutPercent: 0,
                Name: Name,
                Limits: {},
                /// Descripción del segmento
                Description: Description,
                tags: Tags
            }
        };
        /// Sí se habilita la bandera de envío de sms
        if (SendSMS) {
            campaignParams.WriteCampaignRequest.MessageConfiguration.SMSMessage = SMSMessage;
        }
        /// Sí se habilita la bandera de Notificaciones push
        if (SendPush) {
            campaignParams.WriteCampaignRequest.MessageConfiguration.APNSMessage = APNSMessage;
            campaignParams.WriteCampaignRequest.MessageConfiguration.GCMMessage = GCMMessage;
        }
        console.log("Campaign params: ", campaignParams);
        /// Método que crea una campaña para el envío de notificaciones
        return Pinpoint.createCampaign(campaignParams).promise().then(function (data) {
            /// Sí todo salio bien, se imprime y se devuelve la respuesta
            console.log("Campaign created successfully");//, data);
            return data;
        }).catch(function (err) {
            /// Se registra el error y se lanza la excepción
            console.log("Error creating a campaign: ", err);
            throw err;
        });
    },
    /**
     * Método que envía un email a los destinatarios.
     * @param {String} From Dirección de la que se envía el mail
     * @param {String} Subject Título del correo
     * @param {String} HtmlContent Contenido del mensaje en formate html
     * @param {String} TextContent Contenido en texto simple
     * @param {Array} ToAddresses Lista de direcciones de correo a las que se envía el mail
     * @param {Array} CcAddresses Lista de direcciones de correo a las que se envía copia
     * @param {Array} BccAddresses Lista de direcciones de correo a las que se envía copia oculta
     * @returns {Object} Objeto de respuesta
     */
    sendEmail: (From, Subject, HtmlContent, TextContent, ToAddresses, CcAddresses, BccAddresses) => {
        /// Inicializa el mensaje a enviar
        let email = {
            FromEmailAddress: From,
            Destination: {
                ToAddresses: ToAddresses,
                // CcAddresses: CcAddresses,
                // BccAddresses: BccAddresses
            },
            Content: {
                Simple: {
                    Body: {
                        Html: {
                            Data: HtmlContent,
                            Charset: "UTF-8"
                        },
                        Text: {
                            Data: TextContent,
                            Charset: "UTF-8"
                        }
                    },
                    Subject: {
                        Data: Subject,
                        Charset: "UTF-8"
                    }
                }
            }
        };
        /// Sí se encuentras direcciones para copia
        if (CcAddresses != null && CcAddresses.length > 0)
            email.Destination.CcAddresses = CcAddresses;
        /// Sí se encuentran direcciones con copia adjunta
        if (BccAddresses != null && BccAddresses.length > 0)
            email.Destination.BccAddresses = BccAddresses;
        console.log("sendEmail: ", email);
        /// Envía el mail con la configuración especificada
        return PinpointEmail.sendEmail(email).promise().then(function (data) {
            console.log("Emails sent successfully.", data);
            return data;
        }).catch(function (err) {
            console.log("Error sending email: ", err);
            throw err;
        });
    },
    /**
     * Método que obtiene la información de un segmento de Pinpoint
     * @param {Number} SegmentId Id del segmento
     * @returns {Object} Respuesta
     */
    getSegment: async (SegmentId) => {
        return await Pinpoint.getSegment({
            ApplicationId: PinpointAppId,
            SegmentId: SegmentId
        }).promise().then(function (data) {
            console.log("getSegment data: ", data);
            return data;
        }).catch(function (error) {
            console.log("getSegment error: ", error);
            if (error['code'] == 'NotFoundException') {
                return null;
            }
            throw error;
        });
    },
    /**
     * Método que envía un email generado en MIME type a través de Content Raw de pinpoint email
     * @param {String} From Dirección de la que se envía el mail
     * @param {String} RawMessage Contenido del Email en formato MIME
     * @param {Array} ToAddresses Lista de direcciones de correo a las que se envía el mail
     * @param {Array} CcAddresses Lista de direcciones de correo a las que se envía copia
     * @param {Array} BccAddresses Lista de direcciones de correo a las que se envía copia oculta
     */
    sendRawEmail: async (From, RawMessage, ToAddresses, CcAddresses, BccAddresses) => {
        /// Inicializa el mensaje a enviar
        let email = {
            FromEmailAddress: From,
            Destination: {
                ToAddresses: ToAddresses
            },
            Content: {
                Raw: {
                    Data: Buffer.from(RawMessage)
                }
            }
        };
        /// Sí se encuentras direcciones para copia
        if (CcAddresses != null && CcAddresses.length > 0)
            email.Destination.CcAddresses = CcAddresses;
        /// Sí se encuentran direcciones con copia adjunta
        if (BccAddresses != null && BccAddresses.length > 0)
            email.Destination.BccAddresses = BccAddresses;
        console.log("sendEmail: ", email);
        return PinpointEmail.sendEmail(email).promise().then(function (data) {
            console.log("Emails sent successfully.", data);
            return data;
        }).catch(function (err) {
            console.log("Error sending email: ", err);
            return new AWSSESError(err.message, err.stack);
            /// throw err; ---> descomentar sí se desea desplegar el error
        });
    }
}