/**
 * Load the AWS SDK
 */
var AWS = require('aws-sdk');
/**
 * Create a Secrets Manager client
 */
var secretsManagerClient = new AWS.SecretsManager({
    region: process.env.REGION_SECRETS_MANAGER
});
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que gestiona las interacciones con AWS Secrets Manager
 * @author          Hector Trujillo Ruiz
 * @creationDate    26 de Febrero del 2020
 * @class           Clase que gestiona las interacciones con AWS Secrets Manager
 */
let secretsManager = {};
/**
 * Método que obtiene la información del secreto especificado
 * @param {String} secretName Nombre del secreto
 * @returns {Object} Información del secreto
 */
secretsManager.getSecretValue = async (secretName) => {
    // console.log("secretName: ", secretName);
    return secretsManagerClient.getSecretValue({ SecretId: secretName }).promise().then(function (data) {
        let secret = null;
        /// Sí todo salio bien, se imprime y se devuelve la respuesta
        // console.log("getSecretValue: ", data);
        if ('SecretString' in data) {
            secret = data.SecretString;
            // console.log("secret instanceof: ", typeof secret);
            // console.log("secret: ", secret);
        } else {
            let buff = new Buffer(data.SecretBinary, 'base64');
            let decodedBinarySecret = buff.toString('ascii');
            // console.log("decodedBinarySecret: ", decodedBinarySecret);
        }
        return secret;
    }).catch(function (err) {
        /// Se registra el error y se lanza la excepción
        console.log("Error creating a campaign: ", err);
        throw err;
    });
};
/// Exporta el módulo
module.exports = secretsManager;
// In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
// See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
// We rethrow the exception by default.

// secretsManagerClient.getSecretValue({ SecretId: secretName }, function (err, data) {
//     if (err) {
//         if (err.code === 'DecryptionFailureException')
//             // Secrets Manager can't decrypt the protected secret text using the provided KMS key.
//             // Deal with the exception here, and/or rethrow at your discretion.
//             throw err;
//         else if (err.code === 'InternalServiceErrorException')
//             // An error occurred on the server side.
//             // Deal with the exception here, and/or rethrow at your discretion.
//             throw err;
//         else if (err.code === 'InvalidParameterException')
//             // You provided an invalid value for a parameter.
//             // Deal with the exception here, and/or rethrow at your discretion.
//             throw err;
//         else if (err.code === 'InvalidRequestException')
//             // You provided a parameter value that is not valid for the current state of the resource.
//             // Deal with the exception here, and/or rethrow at your discretion.
//             throw err;
//         else if (err.code === 'ResourceNotFoundException')
//             // We can't find the resource that you asked for.
//             // Deal with the exception here, and/or rethrow at your discretion.
//             throw err;
//     }
//     else {
//         // Decrypts secret using the associated KMS CMK.
//         // Depending on whether the secret is a string or binary, one of these fields will be populated.
//         if ('SecretString' in data) {
//             secret = data.SecretString;
//         } else {
//             let buff = new Buffer(data.SecretBinary, 'base64');
//             decodedBinarySecret = buff.toString('ascii');
//         }
//     }

//     // Your code goes here. 
// });