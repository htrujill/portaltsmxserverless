/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Clase que contiene las constantes de la aplicación
 * @author          Hector Trujillo Ruiz
 * @creationDate    08 de Noviembre del 2019
 */
const constants = {
    /**
     * Constantes comunes
     */
    commons: {
    },
    /**
     * Sección para los usuarios
     */
    user: {
        /**
         * Nombre del SP que gestiona los usuarios
         */
        spName: 'CALL appTSMX.dUsuario_SP (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        /**
         * Nombre del SP que se ejecuta para gestionar los invitados de una reservación
         */
        spNameAttendees: 'CALL appTSMX.rInvitados_SP (?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Sección para las ubicaciones
     */
    location: {
        /**
         * Sp que gestinoa la información de las ubicaciones
         */
        spName: 'CALL appTSMX.cUbicacion_SP (?, ?, ?, ?, ?, ?, ?)'
        // spName: 'CALL appTSMX.cUbicacion_SP (?, ?)'
    },
    /**
     * Mapas de la aplicación
     */
    map: {
        /**
         * Nombre del SP a ejecutar
         */
        spName: 'CALL appTSMX.cMapa_SP(?, ?, ?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Menú de la aplicación
     */
    menu: {
        /**
         * Nombre del sp a ejecutar
         */
        spName: 'CALL appTSMX.cOpcion_SP(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Recursos de la aplicación
     */
    resources: {
        /**
         * Nombre del sp a ejecutar
         */
        spName: 'CALL appTSMX.dRecurso_SP (?, ?, ?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Reservación de un item
     */
    reservation: {
        /**
         * Nombre del SP
         */
        spName: 'CALL appTSMX.dReservacion_SP (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        /**
         * Nombre del SP que cancela las reservaciones no confirmadas
         */
        spNameNonConfirmedReservations: 'CALL appTSMX.dCancelaReservacionesTemporales_SP (?, ?)'
    },
    /**
     * Items
     */
    item: {
        /**
         * Nombre del sp a ejecutar
         */
        spName: 'CALL appTSMX.dArticulo_SP (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Recurrencia
     */
    recurrence: {
        /**
         * NOmbre del SP a ejecutar
         */
        spName: 'CALL appTSMX.dRecurrencia_SP (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Atributos
     */
    attributes: {
        /**
         * Nombre del SP a ejecutar
         */
        spNameAttributeTypes: 'CALL appTSMX.buscaTiposDeAtributo_SP (?, ?, ?)',
        /**
         * 
         */
        spNameForValues: 'CALL appTSMX.buscaValoresEnTipoAtributo_SP (?, ?, ?, ?)',
        /**
         * SP para obtener los atributos
         */
        spNameAttributes: 'CALL appTSMX.cAtributo_SP (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    },
    /**
     * Webex
     */
    webex: {
        /**
         * SP que gestiona la información de una webex en base de datos
         */
        spName: 'CALL appTSMX.dWebex_SP (?, ?, ?, ?, ?, ?, ?)'
    }
};
/// Exporta el módulo
module.exports = constants;