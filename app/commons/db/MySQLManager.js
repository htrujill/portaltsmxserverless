/// Manager de KMS
const KMS = require('../AWSServices/KMSManager');
/**
 * MAnager que gestiona las contraseñas de Secrets manager
 */
let SecretsManager = require("../AWSServices/SecretsManager");
/**
 * Error custom para base de datos
 */
const DataBaseError = require('../models/errors/DataBaseError');
/**
 * Cliente de MySQL
 */
const mysql = require('mysql2');
/**
 * Conexión a la base de datos
 */
let globalPool = null;
/**
 * Información de conexión a la base de datos obtenida desde KMS
 */
let DBConnection = null;
/**
 * Pool como promise
 */
let promisePool = null;
/**
 * Método que obtiene la información de conexión a la base de datos
 */
const GetDBInfo = async () => {
    /// Sí no se ha recuperado la información de conexión a la base de datos
    if (DBConnection == null) {
        /// Se desencripta la información de conexión a la base de datos y se separa por ; en un array
        DBConnection = JSON.parse(await SecretsManager.getSecretValue(process.env.DB_SECRET_NAME));
        // console.debug("DB connection information retrieved. ", DBConnection);
        console.debug("DB connection information retrieved.");
    }
    /// Se crea la conexión si no se ha inicializado
    if (globalPool == null) {
        console.info("Creating db connection...");
        let info = {
            host: DBConnection.host,
            user: DBConnection.username,
            database: 'appTSMX',
            password: DBConnection.password,
            waitForConnections: true,
            connectionLimit: 10,
            queueLimit: 0,
            port: DBConnection.port
        };
        /// Inicializa la conexión
        globalPool = mysql.createPool(
            info
        );
        console.info("DB connection created.");
    }
    /// Si es nulo el pool de conexiones
    if (promisePool == null) {
        console.info("Creating Promise wrapped instance of db pool...");
        // now get a Promise wrapped instance of that pool
        promisePool = globalPool.promise();
        console.info("Promise wrapped instance of db pool created.");
    }
};
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Capa de conexión a la base de datos
 * @author          Hector Trujillo Ruiz
 * @creationDate    02 de Mayo del 2019
 * @class Clase que se encarga de la conexión a la base de datos.
 */
let MySQLManager = {};
/**
* Método que ejecuta una sentencia con los parámetros especificados
* @param {String} query Nombre del procedimiento almacenado
* @param {[Any]} params array con los parametros [1,2,'']
* @returns {Object}
*/
MySQLManager.execQueryWithParameters = async (query, params) => {
    try {
        /// Obtiene la información de la base de datos
        await GetDBInfo();
        console.info('query: ', query);
        console.info('params: ', params);
        // query database
        const [rows, fields] = await promisePool.execute(query, params);
        // console.info('params AFTER: ', params);
        console.info('rows: ', rows);
        // console.info('fields: ', fields);
        return rows;
    } catch (err) {
        console.error('TRY CATCH error', err);
        // throw new Error("Internal Error");
        throw new DataBaseError(err.message, err.stack ? err.stack : {});
    }
}
/// Exporta el modulo
module.exports = MySQLManager;