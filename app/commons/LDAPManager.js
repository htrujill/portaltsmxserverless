/**
 * Gestiona la conexión al AD
 */
const activeDirectoryManager = require('activedirectory');
/**
 * Información de conexión al AD
 */
let config = {
    url: 'ldap://10.58.220.248',
    baseDN: 'OU=Users TSMX,OU=Domain Users,DC=ts-mex,DC=com',
    username: 'tsmx_jim@TS-MEX.COM',
    password: 'Yamxr4iF7qj*JVW'
};
/**
 * Inicializa la conexión al ActiveDirectory
 */
let adConnection = new activeDirectoryManager(config);
/**
 * Entidad que contiene la información de un usuario de LDAP
 */
let LdapUser = require('../commons/models/user/LdapUser');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Manager que gestiona las transacciones con el AD
 * @author          Hector Trujillo Ruiz
 * @creationDate    14 de Noviembre del 2019
 * @class Manager que gestiona las transacciones con el AD
 */
let ldapManager = {};
/**
 * Método que busca un usuario en el Active Directory
 * @param {String} searchString Cadena a buscar en el AD
 * @returns {[LdapUser]} Lista de usuarios provenientes del ActiveDirectory
 */
ldapManager.searchUser = (searchString) => {
    console.log('searchString: ', searchString);
    /// Se crea un promise para devolver la información de los usuarios
    return new Promise(function (resolve, reject) {
        adConnection.findUsers(
            `(|(givenName=*${searchString}*)(sn=*${searchString}*)(sAMAccountName=*${searchString}*)(mail=*${searchString}*))`,
            true,
            function (err, users) {
                /// Sí se encuentra un error
                if (err) reject(err);
                /// Sí no se encuentran usuarios
                if ((!users) || (users.length == 0)) {
                    console.log('No users found.')
                    resolve([]);
                } else {
                    /// Lista a devolver
                    let returnList = [];
                    /// Recorre la lista de usuarios
                    for (const key in users) {
                        /// Sí la lista contiene la llave
                        if (users.hasOwnProperty(key)) {
                            /// Se inicializa un nuevo usuario de LDAP y se agrega a la lista
                            returnList.push(new LdapUser(
                                users[key].sAMAccountName,
                                users[key].mail,
                                users[key].displayName
                            ));
                        }
                    }
                    // console.log('findUsers: ' + JSON.stringify(returnList));
                    resolve(returnList);
                }
            }
        );
    });
};
/**
 * Método que obtiene la información de un usuario desde active directory
 * @param {String} username Nombre de usuario a consultar
 * @returns {LdapUser} Información del usuario de LDAP
 */
ldapManager.getUser = (username) => {
    return new Promise(function (resolve, reject) {
        adConnection.findUser(
            username,
            function (err, user) {
                /// Sí se encuentra un error
                if (err) reject(err);
                /// Sí no se encuentran usuarios
                if (!user) {
                    console.log(`The user ${username} could not be found, please verify.`);
                    // resolve({});
                    resolve(null);
                } else {
                    /// Devuelve la información del usuario
                    resolve(new LdapUser(
                        user.sAMAccountName,
                        user.mail,
                        user.displayName
                    ));
                }
            }
        );
    });
};
/**
 * Método que autentica a un usuario en el AD para validar sus credenciales
 * @param {String} username Nombre de usuario
 * @param {String} password Contraseña
 * @returns {Promise} Indica sí el usuario se ha autenticado
 */
ldapManager.authenticate = (username, password) => {
    return new Promise(function (resolve, reject) {
        adConnection.authenticate(
            username + '@TS-MEX.COM',
            password,
            function (err, auth) {
                console.info("auth: ", auth);
                console.info("err: ", err);
                /// Sí se encuentra un error
                if (err) reject(err);
                /// Sí no se encuentran usuarios
                if (auth) {
                    console.log(`The provided credentials are correct.`)
                    resolve({
                        isAuthenticated: true
                    });
                } else {
                    console.log("Wrong credentials for user: ", username);
                    /// Devuelve la información del usuario
                    resolve({
                        isAuthenticated: false
                    });
                }
            }
        );
    });
};
/// Exporta el módulo
module.exports = ldapManager;