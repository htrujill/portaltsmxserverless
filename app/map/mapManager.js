'use strict';
/**
 * Constantes del proyecto
 */
const constants = require('../commons/db/Constants');
/**
 * Manager de base de datos
 */
const MySQLManager = require("../commons/db/MySQLManager");
/**
 * Clase que contiene la información de un mapa
 */
const Map = require("../commons/models/map/Map");
/**
 * Parámetros que se pasan al SP de usuario
 * [0] Opción del SP
 * [1] Id del mapa
 * [2] Id de la ubicación
 * [3] Id del recurso
 * [4] Nombre
 * [5] comentarios
 * [6] Fecha de cambio
 * [7] Usuario que realiza el cambio
 * [8] Status
 */
let params = [
    0,                  /// [0] Opción del SP
    0,                  /// [1] Id del mapa
    0,                  /// [2] Id de la ubicación
    0,                  /// [3] Id del recurso
    '',                 /// [4] Nombre
    '',                 /// [5] Comentarios
    '',                 /// [6] Fecha de cambio
    'LOCALTIME()',      /// [7] Usuario que realiza el cambio
    Buffer.from([1])    /// [8] Status
];
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Manager para obtener la información de los mapas
 * @author          Hector Trujillo Ruiz
 * @creationDate    11 de Noviembre del 2019
 * @class Manager que gestiona la información de los mapas
 */
let mapManager = {};
/**
 * Método que mapea la información de un time de BD a un mapa 
 * @return {Map} Información mapeada
 */
mapManager.mapDBItemToMap = (item) => {
    let bytes = Buffer.from(item.dnEstado);
    return new Map(
        item.pnClave,
        item.dsNombre,
        item.fnUbicación,
        item.dsComentarios,
        item.ddUCambio,
        item.dsUCambio,
        bytes[0],
        null,
        item.dnTipoRecurso,
        item.dsContenido
    );
};
/**
 * Método que obtiene la lista de mapas activos y no activos
 * @returns {[Map]} Lista de mapas
 */
mapManager.getAllMaps = async () => {
    /// Opción del SP -- Obtiene los recursos de una opción
    params[0] = 7;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.map.spName,
        params
    );
    /// Inicializa el valor a devolver
    let returnList = [];
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];
            /// Mapea la información del usuario
            returnList.push(
                //     new Map(
                //     item.pnID,
                //     item.dsNombre,
                //     item.fnUbicación,
                //     item.dsComentarios,
                //     item.ddUCambio,
                //     item.dsUCambio,
                //     bytes[0]
                // )
                mapManager.mapDBItemToMap(item)
            );
        }
    }
    return returnList;
};
/// Exporta el módulo
module.exports = mapManager;
