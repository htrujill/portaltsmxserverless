'use strict';
/**
 * Headers de la respuesta
 */
const commons = require("../commons/models/commonHeaders");
/**
 * Handler para los errores
 */
const errorHandler = require('../commons/ErrorHandler');
/**
 * Respuesta genérica de las lambdas
 */
const commonResponse = require('../commons/models/commonResponse');
/**
 * Manager para la gestion de los mapas
 */
const mapManager = require('./mapManager');
/**
 * Manager que valida la información/parámetros de entrada
 */
const dataValidatorManager = require('../commons/DataValidatorManager');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Handler para obtener la información de los mapas disponibles
 * @author          Hector Trujillo Ruiz
 * @creationDate    28 de Octubre del 2019
 * @class Handler que gestiona la información de los mapas disponibles
 */
let mapHandler = {};
/**
 * Método que obtiene la lista de mapas
 * @param {Object} event Evento que invoca la función
 * @param {Object} context Contexto en el que se ejecuta la función
 * @returns {Object}
 */
mapHandler.getAllMaps = async (event, context) => {
    try {
        /// Obtiene la lista de mapas por ubicación
        let response = await mapManager.getAllMaps();
        console.info("mapHandler.getAllMaps: ", response);
        /// Devuelve la respuesta de la lambda
        return new commonResponse(
            200,
            commons.responseHeaders,
            response
        );
    } catch (error) {
        /// Evalua el error y devuelve la respuesta
        return errorHandler.handleError(error);
    }
};
/// Exporta el módulo
module.exports = mapHandler;
