'use strict';
/**
 * Manager que gestiona la conexión a la base de datos
 */
const MySQLManager = require("../commons/db/MySQLManager");
/**
 * Constantes del proyecto
 */
const constants = require('../commons/db/Constants');
/**
 * Entidad que contiene la información de un recurso
 */
const Resource = require('../commons/models/resources/Resource');
/**
 * Parámetros que se pasan al SP de usuario
 * [0] Opción del SP
 * [1] Id del recurso
 * [2] Id del tipo de recurso
 * [3] Contenido
 * [4] Fecha de actualización
 * [5] Usuario que realiza la acción
 * [6] Id de la opción a la que pertenece
 * [7] Id del atributo
 */
let params = [
    0,                  /// [0] Opción del SP
    0,                  /// [1] Id del recurso
    0,                  /// [2] Id del tipo de recurso
    '',                 /// [3] Contenido
    'LOCALTIME()',      /// [4] Fecha de actualización
    '',                 /// [5] Usuario que realiza la acción
    0,                  /// [6] Id de la opción a la que pertenece
    0,                  /// [7] Id del atributo
    0
];
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Manager que gestiona la información de los usuarios
 * @author          Hector Trujillo Ruiz
 * @creationDate    08 de Noviembre del 2019
 * @class           Manager que gestiona la información de los usuarios
 */
let resourcesManager = {};
/**
 * Método que obtiene la lista de recursos por Id de opción
 */
resourcesManager.getResourcesByOptionId = async (optionId) => {
    /// Opción del SP -- Obtiene los recursos de una opción
    params[0] = 5;
    /// Id de la opción a consultar
    params[6] = optionId;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.resources.spName,
        params
    );
    /// Inicializa el valor a devolver
    let returnList = [];
    let icon = null;
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];
            if (item.dnTipoRecurso == 2) {
                // icon = new Object({
                //     icon: item.dsContenido
                // });
                icon = item.dsContenido;
            } else {
                /// Mapea la información del usuario
                returnList.push(new Resource(
                    item.pnID,
                    item.dnTipoRecurso,
                    item.dsContenido,
                    item.ddUCambio,
                    item.dsUCambio
                ));
            }
        }
    }
    return new Object({
        icon: icon,
        resources: returnList
    });
};
/// Exporta el manager
module.exports = resourcesManager;