'use strict';
/**
 * Manager de base de datos
 */
const MySQLManager = require("../commons/db/MySQLManager");
/**
 * Modelo para la ubicación
 */
const Location = require('../commons/models/location/Location');
/**
 * Constantes del proyecto
 */
const constants = require('../commons/db/Constants');
/**
 * Modelo para la configuración de la ubicación
 */
const Configuration = require('../commons/models/location/Configuration');
/**
 * Manager para los atributos de un item
 */
const attributeManager = require('./attributesManager');
/**
 * Parámetros del sp
 */
let params = [
    0,
    0,
    Buffer.from([1]),
    '',
    '',
    'LOCALTIME()',
    ''
];
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Handler para obtener la información de las ubicaciones disponibles
 * @author          Hector Trujillo Ruiz
 * @creationDate    05 de Noviembre del 2019
 * @class Handler que gestiona la información de las ubicaciones disponibles
 */
let locationManager = {};
/**
 * Método que crea una ubicación
 * @param {Location} location Información de la ubicación
 * @returns {Object} Confirmación de que se ha creado la ubicación
 */
locationManager.createLocation = async (location) => {
    /// Parámetros del SP
    params[0] = 1;
    params[2] = Buffer.from([1]);
    params[3] = location.getName();
    params[4] = location.getComments();
    params[5] = 'LOCALTIME()';
    params[6] = location.getUpdatedBy();
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.location.spName,
        params
    );
    console.log('response: ', response);
    return {
        created: true
    };
};
/**
 * Método que actualiza una ubicación
 * @param {Location} location Información de la ubicación
 * @returns {Object} Confirmación de que se ha actualizado la ubicación
 */
locationManager.updateLocation = async (location) => {
    /// Parámetros del SP
    params[0] = 2;
    params[1] = location.getId();
    params[2] = Buffer.from([location.getStatus()]);
    params[3] = location.getName();
    params[4] = location.getComments();
    params[5] = location.getLastUpdateAsDate();
    params[6] = location.getUpdatedBy();
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.location.spName,
        params
    );
    console.log('response: ', response);
    return {
        updated: true
    };
};
/**
 * Método que elimina una ubicación
 * @param {Number} locationId Id de la ubicación
 * @returns {Object} Confirmación de que se ha eliminado la ubicación
 */
locationManager.deleteLocation = async (locationId) => {
    /// Parámetros del SP
    params[0] = 3;
    params[1] = locationId;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.location.spName,
        params
    );
    console.log('response: ', response);
    return {
        deleted: true
    };
};
// /**
//  * Método que obtiene la lista de ubicaciones disponibles
//  * @returns {[Location]} Lista de ubicaciones
//  */
// locationManager.getLocations = async () => {
//     /// Parámetros del SP
//     params[0] = 5;
//     /// Ejecuta el SP con los parámetros especificados
//     let response = await MySQLManager.execQueryWithParameters(
//         constants.location.spName,
//         params
//     );
//     /// Inicializa el valor a devolver
//     let returnValue = [];
//     /// Si se encontró información
//     if (response != null && response.length > 0 && response[0].length > 0) {
//         /// Por cada elemento encontrado
//         for (let index = 0; index < response[0].length; index++) {
//             /// Obtiene el item de la respuesta
//             const item = response[0][index];
//             ///const status = Buffer.from(item.dnEstado);
//             returnValue.push(
//                 await locationManager.getLocationById(item.pnID)
//             );
//         }
//     }
//     return returnValue;
// };
/**
 * Método que obtiene la lista de ubicaciones activas y no activas
 * @returns {[Location]} Lista de ubicaciones
 */
locationManager.getAllLocations = async () => {
    /// Parámetros del SP
    params[0] = 6;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.location.spName,
        params
    );
    /// Inicializa el valor a devolver
    let returnValue = [];
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];
            ///const status = Buffer.from(item.dnEstado);
            returnValue.push(
                await locationManager.getLocationById(item.pnID)
            );
        }
    }
    return returnValue;
};
/**
 * Método que obtiene una ubicación por Id
 * @param {Number} locationId Id de la ubicación
 * @returns {Location} Información de la ubicación
 */
locationManager.getLocationById = async (locationId) => {
    /// Parámetros del SP
    params[0] = 4;
    params[1] = locationId;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.location.spName,
        params
    );
    /// Inicializa la respuesta
    let returnValue = null;
    /// Inicializa la lista de configuraciones
    let configurations = [];
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];
            const status = Buffer.from(item.dnEstado);
            /// Asigna el valor de la ubicación
            returnValue = new Location(
                item.pnID,
                item.dsNombre,
                item.dsComentarios,
                item.ddUCambio,
                item.dsUCambio,
                status[0]
            );
            /// Por cada row encontrado de configuración se agrega a la lista de configuraciones
            configurations.push(new Configuration(
                item.dnMinutos,
                item.fnTipoArticulo,
                item.dsComentariosTipoArticulo,
                item.dsNombreTipoArticulo,
                await attributeManager.getAttributeTypesByItemAndLocation(item.fnTipoArticulo, item.pnID)
            ));
        }
        /// Se agrega la lista de configuraciones a la información de la ubicación
        returnValue.setConfigurations(configurations);
    }
    /// Se devuelve el resultado
    return returnValue;
}
/// Exporta el módulo
module.exports = locationManager;
