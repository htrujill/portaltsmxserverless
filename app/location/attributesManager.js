'use strict';
/**
 * Manager de base de datos
 */
const MySQLManager = require("../commons/db/MySQLManager");
/**
 * Modelo para el tipo de atributo
 */
const AttributeType = require('../commons/models/location/AttributeType');
/**
 * Modelo para un atributo
 */
const Attribute = require('../commons/models/location/Attribute');
/**
 * Modelo para los valores de los tipos de atributos
 */
const AttrValues = require('../commons/models/location/AttrValues');
/**
 * Constantes del proyecto
 */
const constants = require('../commons/db/Constants');
/**
 * Parámetros que se pasan al sp de los attributos
 * [0] Opción del SP
 * [1] Id del item
 * [2] Id de la ubicación
 */
let params = [
    0,
    0,
    0
];
/**
 * [0]  Opción del SP
 * [1]  Id del atributo
 * [2]  Id del tipo de item
 * [3]  Id del Tipo de atributo
 * [4]  Valor mínimo
 * [5]  Valor máximo
 * [6]  Nombre
 * [7]  Descripción
 * [8]  Fecha de cambio
 * [9]  Usuario que actualiza
 * [10] Id del artículo
 */
let attributeParams = [
    0,                  /// [0]  Opción del SP
    0,
    0,
    0,
    0,
    0,
    '',                 /// [6]  Nombre
    '',                 /// [7]  Descripción
    'LOCALTIME ()',     /// [8]  Fecha de cambio
    '',                 /// [9]  Usuario que actualiza
    0                   /// [10] Id del artículo
];
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Manager para obtener los atributos de un item
 * @author          Hector Trujillo Ruiz
 * @creationDate    13 de Enero del 2020
 * @class           Manager para obtener los atributos de un item
 */
let attributeManager = {};
/**
 * Método que obtiene una ubicación por Id
 * @param {Number} itemTypeId Id del tipo de item
 * @param {Number} locationId Id de la ubicación
 * @returns {[AttributeType]} Lista de tipos de atributos
 */
attributeManager.getAttributeTypesByItemAndLocation = async (itemTypeId, locationId) => {
    /// Parámetros del SP
    params[0] = 1;
    params[1] = itemTypeId;
    params[2] = locationId;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.attributes.spNameAttributeTypes,
        params
    );
    /// Inicializa la respuesta
    let returnList = [];
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];
            console.info('getAttributesByItemAndLocation.item: ', item);
            /// Por cada row encontrado de atributo se agrega a la lista de atributos
            returnList.push(new AttributeType(
                item.pnID,
                item.dnTipoAtributo,
                item.dnValorMinimo,
                item.dnValorMaximo,
                item.dsNombre,
                item.dsDescripcion,
                (item.dnTipoAtributo == 5 || item.dnTipoAtributo == 6) ? await attributeManager.getAttrValuesByLocationandItemTypeAndAttribute(locationId, itemTypeId, item.pnID) : []
            ));
        }
    }
    /// Se devuelve el resultado
    return returnList;
};
/**
 * Método que obtiene los valores posible de los atributos
 * @param {Number} locationId Id de la ubicación
 * @param {Number} itemTypeId Id del tipo de artículo
 * @param {Number} attributeId Id del atributo
 * @returns {[AttrValues]} Lista de valores
 */
attributeManager.getAttrValuesByLocationandItemTypeAndAttribute = async (locationId, itemTypeId, attributeId) => {
    console.info('getAttrValuesByLocationandItemTypeAndAttribute');
    /// Parámetros del SP
    let parameters = [
        1,
        locationId,
        itemTypeId,
        attributeId
    ];
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.attributes.spNameForValues,
        parameters
    );
    /// Inicializa la respuesta
    let returnList = [];
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];
            /// Por cada row encontrado de atributo se agrega a la lista de atributos
            returnList.push(new AttrValues(
                item.dnValor,
                item.dsValor,
                item.dsNombre
            ));
        }
    }
    /// Se devuelve el resultado
    return returnList;
};
/**
 * Método que obtiene los atributos de un item
 * @param {Number} itemId Id del item
 * @returns {[Attribute]} Lista de atributos del item
 */
attributeManager.getAttributesByItem = async (itemId) => {
    console.log('attributeManager.getAttributesByItem.itemId: ', itemId);
    /// Asigna los parámetros 
    attributeParams[0] = 6;
    attributeParams[10] = itemId;
    /// Ejecuta el SP con los parámetros especificados
    let response = await MySQLManager.execQueryWithParameters(
        constants.attributes.spNameAttributes,
        attributeParams
    );
    /// Inicializa la respuesta
    let returnList = [];
    /// Si se encontró información
    if (response != null && response.length > 0 && response[0].length > 0) {
        /// Por cada elemento encontrado
        for (let index = 0; index < response[0].length; index++) {
            /// Obtiene el item de la respuesta
            const item = response[0][index];

            /// Por cada row encontrado de atributo se agrega a la lista de atributos
            returnList.push(new Attribute(
                item.pnID,
                item.fnTipoArticulo,
                item.dnTipoAtributo,
                item.dnValorMinimo,
                item.dnValorMaximo,
                item.dsNombre,
                item.dsDescripcion,
                item.ddUCambio,
                item.dsUCambio,
                item.dnTipoRecurso,
                item.dsContenido,
                item.dnValor,
                item.dsValor
            ));
        }
    }
    /// Se devuelve el resultado
    return returnList;
};
/// Exporta el módulo
module.exports = attributeManager;
