'use strict';
/// Headers de la respuesta
const commons = require("../commons/models/commonHeaders");
/// Handler para los errores
const errorHandler = require('../commons/ErrorHandler');
/// Respuesta genérica de las lambdas
const commonResponse = require('../commons/models/commonResponse');
/// Handler para las ubicaciones
const locationManager = require('./locationManager');
/**
 * Modelo para la ubicación
 */
const Location = require('../commons/models/location/Location');
/**
 * Manager que valida la información/parámetros de entrada
 */
const dataValidatorManager = require('../commons/DataValidatorManager');
/**
 * @copyright       T-Systems México S.A. de C.V
 * @description     Handler para obtener la información de las ubicaciones
 * @author          Hector Trujillo Ruiz
 * @creationDate    28 de Octubre del 2019
 * @class Handler que gestiona la información de las ubicaciones
 */
let locationHandler = {};
/**
 * Método que crea una ubicación
 * @param {Object} event Evento que invoca la función
 * @param {Object} context Contexto en el que se ejecuta la función
 * @returns {Object}
 */
locationHandler.createLocation = async (event, context) => {
    try {
        let body = JSON.parse(event.body);
        console.log('body: ', body);
        let response = await locationManager.createLocation(new Location(
            null,
            body.name,
            body.comments,
            null,
            dataValidatorManager.getAuthUsername(event),
            1,
            null
        ));
        console.info('locationHandler.response: ', response);
        /// Devuelve la respuesta de la lambda
        return new commonResponse(200, commons.responseHeaders, response);
    } catch (error) {
        /// Evalua el error y devuelve la respuesta
        return errorHandler.handleError(error);
    }
};
/**
 * Método que actualiza una ubicación
 * @param {Object} event Evento que invoca la función
 * @param {Object} context Contexto en el que se ejecuta la función
 * @returns {Object}
 */
locationHandler.updateLocation = async (event, context) => {
    try {
        let body = JSON.parse(event.body);
        console.log('body: ', body);
        let location = await locationManager.getLocationById(dataValidatorManager.validateInteger(event.pathParameters.locationId));
        /// Sí no existe la ubicación
        if (!location) {
            throw new EntityNotFoundError(`The location with id ${event.pathParameters.locationId} could not be found, please verify.`);
        }
        location.setName(body.name);
        location.setComments(body.comments);
        location.setUpdatedBy(dataValidatorManager.getAuthUsername(event));
        location.setStatus(parseInt(body.status));
        /// Actualiza la información de una ubicación
        let response = await locationManager.updateLocation(location);
        console.info('locationHandler.updateLocation: ', response);
        /// Devuelve la respuesta de la lambda
        return new commonResponse(200, commons.responseHeaders, response);
    } catch (error) {
        /// Evalua el error y devuelve la respuesta
        return errorHandler.handleError(error);
    }
};
/**
 * Método que elimina una ubicación
 * @param {Object} event Evento que invoca la función
 * @param {Object} context Contexto en el que se ejecuta la función
 * @returns {Object}
 */
locationHandler.deleteLocation = async (event, context) => {
    try {
        let response = await locationManager.deleteLocation(dataValidatorManager.validateInteger(event.pathParameters.locationId));
        console.info('locationHandler.deleteLocation: ', response);
        /// Devuelve la respuesta de la lambda
        return new commonResponse(200, commons.responseHeaders, response);
    } catch (error) {
        /// Evalua el error y devuelve la respuesta
        return errorHandler.handleError(error);
    }
};
/**
 * Método que obtiene la lista de ubicaciones disponibles
 * @param {Object} event Evento que invoca la función
 * @param {Object} context Contexto en el que se ejecuta la función
 * @returns {Object}
 */
locationHandler.getAllLocations = async (event, context) => {
    try {
        let locations = await locationManager.getAllLocations();
        console.info('locationHandler.getAllLocations: ', locations);
        /// Devuelve la respuesta de la lambda
        return new commonResponse(200, commons.responseHeaders, locations);
    } catch (error) {
        /// Evalua el error y devuelve la respuesta
        return errorHandler.handleError(error);
    }
};
/// Exporta el módulo
module.exports = locationHandler;
